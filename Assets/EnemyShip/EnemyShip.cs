using Godot;
using System;
using System.Collections.Generic;

public class EnemyShip : Area2D
{
    [Export]
    private int MoveSpeed = 40;

    

    public void _on_EnemyShip_body_entered(PhysicsBody2D body){
        if (body.IsInGroup("Player")){
            PackedScene shipBattleScene = (PackedScene)ResourceLoader.Load("res://Assets/ShipBattle/ShipBattle.tscn");

            GetTree().ChangeSceneTo(shipBattleScene);
        }      
    }
}

