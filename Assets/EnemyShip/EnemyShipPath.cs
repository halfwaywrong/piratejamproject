using Godot;
using System;

public class EnemyShipPath : Path2D
{
    [Export]
    private int pathDuration;

    private PathFollow2D follow;

    private Tween tween;

    public override void _Ready()
    {
        follow = (PathFollow2D)GetNode("PathFollow2D");

        tween = new Tween();
        AddChild(tween);
        tween.InterpolateProperty(follow,
                                "unit_offset",
                                0,
                                1,
                                pathDuration,
                                Tween.TransitionType.Linear,
                                Tween.EaseType.InOut);
        tween.Repeat = true;
        tween.Start();
    }

}
