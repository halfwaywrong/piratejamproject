using Godot;
using System;

public class ZombieCaptain : Area2D
{
    [Export]
    private int speed = (int)GD.RandRange(40,70);

    private Vector2 initialPos;
    private Vector2 direction;
    private AnimationPlayer anim;

    public override void _Ready()
    {
        anim = (AnimationPlayer)GetNode("anim");

        initialPos = Position;
        direction = new Vector2((float)GD.RandRange(-1,1),(float)GD.RandRange(-1,1));
        anim.Play("walkDown");
    }

    public override void _Process(float delta)
    {
        Position += direction * speed * delta;
    }

    public void _on_ZombieCaptain_body_entered(PhysicsBody2D body){
        if (body.IsInGroup("sides")){
            direction = new Vector2(-direction.x, direction.y);
        }
        else if (body.IsInGroup("top")){
            direction = new Vector2(direction.x, -direction.y);
        }
    }

    public void Die(){
        QueueFree();
    }
}
