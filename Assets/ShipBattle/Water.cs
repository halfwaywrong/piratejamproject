using Godot;
using System;

public class Water : ParallaxBackground
{
    public override void _Process(float delta)
    {
        ScrollOffset = new Vector2(0, ScrollOffset.y + 10 * delta);
    }
}
