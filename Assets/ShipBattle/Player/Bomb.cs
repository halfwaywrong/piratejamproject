using Godot;
using System;

public class Bomb : KinematicBody2D
{
    PackedScene Explosion = ResourceLoader.Load("res://Assets/ShipBattle/Player/Explosion.tscn") as PackedScene;
    private AnimationPlayer anim;
    private Area2D collider;

    private int speed = 100;
    private Vector2 endPoint;
    
    public override void _Ready()
    {
        anim = (AnimationPlayer)GetNode("anim");
        collider = (Area2D)GetNode("collider");

        anim.Play("idle");
    }

    public void init(Vector2 _endPoint){
        endPoint = _endPoint;
    }

    public override void _PhysicsProcess(float delta)
    {
        if (((int)endPoint.x - (int)Position.x) > 0){
            var velocity = Vector2.Zero;
            velocity = Position.DirectionTo(endPoint) * speed;
            velocity = MoveAndSlide(velocity);
        } else {
            Explosion b = (Explosion)Explosion.Instance();
            b.Position = GlobalPosition;
            GetParent().AddChild(b);

            foreach(Area2D body in collider.GetOverlappingAreas()){
                if (body.IsInGroup("zombie")){
                    var zombie = (ZombieCaptain)body;
                    zombie.Die();
                }
            }
            
            QueueFree();
        }
    }
}
