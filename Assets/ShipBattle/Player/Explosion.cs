using Godot;
using System;

public class Explosion : Sprite
{
    private AnimationPlayer anim;

    public override void _Ready()
    {
        anim = (AnimationPlayer)GetNode("anim");
        anim.Play("explode");
    }
}
