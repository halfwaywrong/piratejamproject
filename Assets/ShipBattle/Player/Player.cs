using Godot;
using System;

public class Player : Sprite
{
    PackedScene Bomb = ResourceLoader.Load("res://Assets/ShipBattle/Player/Bomb.tscn") as PackedScene;

    private int speed = 70;
    private int crosshairSpeed = 100;
    private string curAnim = "idle";

    private AnimationPlayer anim;
    private Sprite crosshair;
    private Node2D bombSpawnPoint;
    private float crosshairInitialX;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        anim = (AnimationPlayer)GetNode("anim");
        crosshair = (Sprite)GetNode("crosshair");
        bombSpawnPoint = (Node2D)GetNode("bombSpawnPoint");

        //set the crosshair initial position to a variable
        crosshairInitialX = crosshair.Position.x;
    }

    public override void _Process(float delta)
    {
        #region movement
        var newAnim = curAnim;

        var direction = GetDirection();
        var yPos = Position.y;

        if (direction != 0){
            newAnim = "walk";
        }else{
            newAnim = "idle";
        }

        if (!Input.IsActionPressed("bomb")){
            
            if ((direction > 0 && Position.y <= 200)
                    || (direction < 0 && Position.y >= 20)){
                Position = new Vector2(Position.x, yPos += direction * speed * delta);
            }
        }else{
            newAnim = "idle";
        }

        if (newAnim != curAnim){
            anim.Play(newAnim);
        }
        curAnim = newAnim;
        #endregion

        #region bomb
            if (Input.IsActionPressed("bomb")){
                crosshair.Position = new Vector2(crosshair.Position.x + (crosshairSpeed*delta), crosshair.Position.y);
            }

            if (Input.IsActionJustReleased("bomb")){
                //drop a bomb
                Bomb b = (Bomb)Bomb.Instance();
                b.Position = bombSpawnPoint.GlobalPosition;
                b.init(crosshair.GlobalPosition);
                Owner.AddChild(b);
                crosshair.Position = new Vector2(crosshairInitialX, crosshair.Position.y);
            }
        #endregion
    }


    private int GetDirection(){

        if (Input.IsActionPressed("accelerate")){
            return -1;
        }else if (Input.IsActionPressed("brake")){
            return 1;
        }
        else{
            return 0;
        }


    }
}
