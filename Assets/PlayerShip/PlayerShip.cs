using Godot;
using System;

public class PlayerShip : KinematicBody2D
{
    private Sprite health1;
    private Sprite health2;
    private Sprite health3;

    private int wheelBase = 8; //distance from front to rear wheel
    private int steeringAngle = 5;
    private int enginePower = 60;
    private Vector2 acceleration = Vector2.Zero; 
    private float friction = -0.2f;
    private float drag = -0.002f;
    private float braking = -45;
    private float maxSpeedReverse = 5;
    private int health = 3;
    
    private Vector2 velocity = Vector2.Zero;
    private float steerAngle;

    public override void _Ready(){
        health1 = (Sprite)GetNode("CanvasLayer/Panel/1");
        health2 = (Sprite)GetNode("CanvasLayer/Panel/2");
        health3 = (Sprite)GetNode("CanvasLayer/Panel/3");
    }

    public override void _PhysicsProcess(float delta)
    {
        acceleration = Vector2.Zero;
        GetInput();
        CalculateSteering(delta);
        ApplyFriction();
        velocity += acceleration * delta;
        velocity = MoveAndSlide(velocity);
    }

    public void Hit(){
        health -= 1;
        
        switch (health)
        {
            case 2:
                health3.Hide();
                break;
            case 1:
                health2.Hide();
                break;
            case 0:
                health1.Hide();
                break;
            default:
                GetTree().ReloadCurrentScene();
                break;
        }
    }

    private void GetInput(){
        int turn = 0;
        if (Input.IsActionPressed("steer_right")){
            turn += 1;
        }

        if (Input.IsActionPressed("steer_left")){
            turn -=1;
        }

        steerAngle = turn * Mathf.Deg2Rad(steeringAngle);

        if (Input.IsActionPressed("accelerate")){
            acceleration = Transform.x * enginePower;
        }

        if (Input.IsActionPressed("brake")){
            acceleration = Transform.x * braking;
        }
    }

    private void CalculateSteering(float delta){

        var rearWheel = Position - Transform.x * wheelBase / 2.0f;
        var frontWheel = Position + Transform.x * wheelBase / 2.0f;
        
        rearWheel -= velocity.Rotated(steerAngle) * delta;
        frontWheel += velocity * delta;
        
        var newHeading = (frontWheel - rearWheel).Normalized();
        var d = newHeading.Dot(velocity.Normalized());

        if (d > 0){
            velocity = newHeading * velocity.Length();
        }
        if (d < 0){
            velocity = -newHeading * Mathf.Min(velocity.Length(), maxSpeedReverse);
        }        
        Rotation = newHeading.Angle();
    }

    private void ApplyFriction(){
        var frictionForce = velocity * friction;
        var dragForce = velocity * velocity.Length() * drag;

        if (velocity.Length() < 100){
            frictionForce *= 3;
        }
        acceleration += dragForce + frictionForce;
    }

}
