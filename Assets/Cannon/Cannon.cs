using Godot;
using System;

public class Cannon : Node2D
{
    PackedScene Bullet = ResourceLoader.Load("res://Assets/Cannon/Bullet.tscn") as PackedScene;

    private Node2D SpawnPoint;
    private Timer ShotTimer;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        SpawnPoint = (Node2D)GetNode("SpawnPoint");
        ShotTimer = (Timer)GetNode("ShotTimer");
        
    }

    public void _on_shotTimer_timeout(){
        Shoot();
        ShotTimer.Start();
    }

    private void Shoot(){
        Bullet b = (Bullet)Bullet.Instance();
        AddChild(b);
        b.Transform = SpawnPoint.Transform;
    }
}
