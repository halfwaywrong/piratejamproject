using Godot;
using System;

public class Bullet : Area2D
{
    PackedScene BulletExplosion = ResourceLoader.Load("res://Assets/Cannon/BulletExplosion.tscn") as PackedScene;

    private int speed = 50;

    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _PhysicsProcess(float delta)
    {
        Position += Transform.x * speed * delta;
    }

    public void _on_Bullet_body_entered(PhysicsBody2D body){
        if (body.IsInGroup("Player")){
            PlayerShip playerShip = (PlayerShip) body;
            playerShip.Hit();
            BulletExplosion b = (BulletExplosion)BulletExplosion.Instance();
            b.Transform = Transform;
            GetNode("../").AddChild(b);
            QueueFree();
        }        
    }

    public void _on_lifeTimer_timeout() {
        QueueFree();
    }
}
