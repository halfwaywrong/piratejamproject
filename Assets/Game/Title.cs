using Godot;
using System;

public class Title : Node2D
{
    private Node2D titleLayout;
    private Node2D credits;

    public override void _Ready()
    {
        titleLayout = (Node2D)GetNode("CanvasLayer/titleLayout");
        credits = (Node2D)GetNode("CanvasLayer/credits");

        titleLayout.Show();
        credits.Hide();
    }

    public void _on_btnCredits_pressed(){
        credits.Show();
        titleLayout.Hide();
    }

    public void _on_btnTitle_pressed(){
        credits.Hide();
        titleLayout.Show();
    }

    public void _on_btnPlay_pressed(){
        PackedScene shipBattleScene = (PackedScene)ResourceLoader.Load("res://Assets/Game/Game.tscn");
        GetTree().ChangeSceneTo(shipBattleScene);
    }


}
