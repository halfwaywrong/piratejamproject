using Godot;
using System;

public class Ending : Node
{
    public void _on_Button_pressed(){
        PackedScene shipBattleScene = (PackedScene)ResourceLoader.Load("res://Assets/Game/Title.tscn");
        GetTree().ChangeSceneTo(shipBattleScene);
    }
}
