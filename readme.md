The Dutchmans Last Flight
A game by Michael Henderson aka Halfway Wrong.
Developed for the OGA Summer Jam 2020

----------------------
Controls when sailing
----------------------
Move - arrow keys or WASD.
To battle the Flying Dutchman, collide with it.

----------------------
Controls when bombing
----------------------
Move - Up/Down arrows, or W/S.
Throw Bomb - Hold Space to aim


Credits

Pique La Baleine
https://opengameart.org/content/pique-la-baleine-sea-shanty-chiptune

Drunken Sailor
https://opengameart.org/content/opl2-unkown-drunken-sailor

Ship
Kenney.nl https://opengameart.org/content/pirate-pack-190

Overworld Tiles
Buch - https://opengameart.org/content/overworld-tiles-0

Explosion effect
LIXING - https://opengameart.org/content/explosion-effect-pixel-art

Ship Battle
Chasersgaming - https://opengameart.org/content/rpg-pirate-ship-tile-set

Ship Battle water
qubodup - https://opengameart.org/content/pastel-resources-tiles-96x96

Zombie Pirates
Reemax - https://opengameart.org/content/pirate-zombie-and-skeleton-32x48

Ship Battle Player and Bombs
Pixel Frog - https://opengameart.org/content/pirate-bomb

Crosshair
fluxord https://opengameart.org/content/20-crosshairs-for-re

Cheerful 2 and Cheerful 3 songs
Bruno Belotti (Submitted by qubodup)
https://opengameart.org/content/cheerful-2-la-spensierata-polka-loop
https://opengameart.org/content/cheerful-3-nel-giardino-dello-zar-polka-loop

Font
Pix3M https://opengameart.org/content/retro-deco-bitmap-font